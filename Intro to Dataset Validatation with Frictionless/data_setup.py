"""
Data Setup

This file is used to download the sample dataset and intentionally 
introduce data quality issues for demonstration purposes.
"""
from pathlib import Path
import random
import urllib.request
from urllib.parse import urlparse

import pandas as pd

URL = "https://web.stanford.edu/class/archive/cs/cs109/cs109.1166/stuff/titanic.csv"

def download_file(url: str, target_file: str = None) -> str:
    if target_file is None:
        parsed_url = urlparse(url)
        target_file = parsed_url.path.split("/")[-1]
        
    local_filename, response = urllib.request.urlretrieve(url, target_file)
    return local_filename


def prepare_data(local_filename: str):
    # Load the data from the local file
    df = pd.read_csv(local_filename)

    # Introduce quality issues
    df.loc[618, "Survived"] = 10
    df.loc[741, "Fare"] = -71
    df.loc[337, "Fare"] = 3355
    df.loc[720, "Name"] = None
    df.loc[271, "Name"] = "Miss. Susan Parker Ryerson"
    df.loc[863, "Name"] = "Washington Augustus Roebling, Mr."
    df.loc[193, "Name"] = df.loc[193, "Name"] + """. From Wikipedia: Margaret Brown (née Tobin; July 18, 1867 – October 26, 1932), posthumously known as "The Unsinkable Molly Brown", was an American socialite and philanthropist. She was a passenger on the RMS Titanic which sank in 1912 and she unsuccessfully urged the crew in Lifeboat No. 6 to return to the debris field to look for survivors.

    During her lifetime, her friends called her "Maggie", but by her death, obituaries referred to her as the "Unsinkable Mrs. Brown". Gene Fowler referred to her as "Molly Brown" in his 1933 book Timberline. The following year, she was referred to as "The Unsinkable Mrs. Brown" and "Molly Brown" in newspapers. """
    df.loc[165, "Age"] = None
    df.loc[504, "Age"] = "43.5"
    df.loc[536, "Age"] = "unknown"
    df.loc[df.Pclass == 2, "Survived"] = df.loc[df.Pclass == 2, "Survived"].apply(lambda x: str(bool(x))[0])
    
    sibsp_map = {
        0: "-",
        1: 1,
        2: 2,
        3: 3
    }
    df.loc[df.Pclass == 1, "Siblings/Spouses Aboard"] = df.loc[df.Pclass == 1]["Siblings/Spouses Aboard"].map(sibsp_map)

    def perturb_value(value):
        p = random.random()
        if p > 0.25:
            return value
        elif p > 0.05:
            return value.title()
        else:
            return value[0].upper()

    random.seed(0)
    df.loc[df.Pclass == 1, "Sex"] = df[df.Pclass == 1].Sex.apply(perturb_value)

    # Write out a file for each class
    df[df.Pclass == 1].to_csv("titanic_first_class.csv", index=None)
    df[df.Pclass == 2].sort_values("Age").to_csv("titanic_second_class.csv", index=None)
    df[df.Pclass == 3].to_csv("titanic_third_class.csv", index=None)
    
def download_and_prepare_data(url):
    local_file = download_file(url)
    prepare_data(local_file)
    
if __name__ == "__main__":
    download_and_prepare_data(URL)